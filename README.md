# Sayeye
 Sayeye Suite is a set of software co-designed for people suffering from Rett's syndrome characterized by an innovative way of interaction between care-giver and care-receiver, both equipped with an instrument on their own device, and through the use of an eyetracker (which allows you to track the look of the subject and to determine which point on the screen is watching).


Sayeye is an open source software and accessible to everyone, co-designed by designers, developers, university researchers, families and therapists. Sayeye promotes communication and interactions between care-givers and care-receivers.
The system integrates different technologies (mobile applications, cloud services and adaptive algorithms) to provide an innovative, comprehensive and easy-to-use service.


The software is a fork of the project Amelie. Amelie was born from an idea of Associazione Italiana Rett - AIRETT Onlus and Opendot S.r.l., and was designed and developed by Opendot S.r.l., with the essential contribution of Associazione Italiana Rett - AIRETT Onlus.

This repository provides the Inno Setup installer script and a helper script to collect all the resources needed to build the installer.


# Sayeye-installer

This package is intended to create an installer for the desktop Sayeye software suite. It is made with the popular [Inno Setup](http://www.jrsoftware.org/) software, using tis scripting language. In order to edit the installer procedure, please install Inno Setup and open the sayeye.iss file with it.

Requirements
---

- Inno setup software, the scripting also uses the [Inno Download Plugin](https://mitrichsoftware.wordpress.com/inno-setup-tools/inno-download-plugin/), please install it following the instructions on the website.
- Git Bash.

Steps performed by the installer:
---
- Check for an already installed copy of the Tobii Eyetracker software. If it's not installed, the setup quits.
- Check for an already installed copy of the Docker Desktop software. If it's not installed, the setup quits.
- Copy the necessary files composing the Sayeye software suite.
- Run Docker Desktop, and wait for it to be ready.
- Build the docker virtual machine.
- Run scripts for preparing the local environment.
- Create the windows menu shortcuts.

The procedure also creates the uninstaller, which can be accessed like any other uninstall executable in the appropriate Windows menu.  

## Development info: How to run the installer build

First of all, create a root directory for the SayEye suite, such as `sayeye-suite-development`.

### Clone all the repositories that compose the SayEye suite.

Inside the development directory, clone this repository (`sayeye-installer`), and the following ones:
- `sayeye-communicator`
- `sayeye-controller`
- `sayeye-server`
- `sayeye-assets`

### Build the communicator and controller

Follow the README files of each respective project. At the end of the process, the following directories should be correctly populated:
- `./sayeye-communicator/dist/`
- `./sayeye-controller/sayeye-controller/bin/Release/`

### Run the preparation script

- Run the script `prepare_installer_files.sh`, using this repository's root as working directory. If you are using git-bash on windows, simply  cd to to this repository, and do:
```
bash prepare_installer_files.sh
``` 

- The script assumes that the previously mentioned repositories and builds exist and are located in the parent directory.

- The script creates the directory `../sayeye_installer_sources/` and populates it with all the resources needed to build a new installer.

### Important information regarding private keys

If SayEye is to be installed with the cloud server support, keys and proper urls are to be added to the `application.yml` file, after it has been imported to the directory `../sayeye_installer_sources/`.

### Run the inno setup software

If the Inno software is installed, just double click on the sayeye.iss file and run a build. This should build a new installer.
