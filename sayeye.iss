; Uncomment one of following lines, if you haven't checked "Add IDP include path to ISPPBuiltins.iss" option during IDP installation:

[Setup]
AppName                = Sayeye Suite
AppVersion             = 0.8.2
DefaultDirName         = {userappdata}\SayeyeSuite
DisableDirPage         = Yes
DefaultGroupName       = Sayeye Suite
ArchitecturesAllowed   = x64
ArchitecturesInstallIn64BitMode = x64
PrivilegesRequired     = admin 
; Size of files to download:
ExtraDiskSpaceRequired = 1048576
OutputDir              = userdocs:Inno Setup Installer Output
OutputBaseFilename     = SayeyeInstaller
AlwaysRestart          = yes
ChangesEnvironment     = yes
AppPublisher           = OpenDot
AppPublisherURL        = http://www.opendotlab.it/

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: it; MessagesFile: "compiler:Languages\Italian.isl"

[CustomMessages]
it.wsl2 = Installazione dei file necessari. Questa operazione potrebbe durare diversi minuti.
en.wsl2 = Installing necessary files. This may take minutes to complete.
it.deps = Installazione delle dipendenze software.
en.deps = Installing software dependencies.

[Icons]
Name: "{group}\{cm:UninstallProgram,My Program}"; Filename: "{uninstallexe}"
Name: "{group}\sayeye controller"; Filename: "{app}\sayeyeController.exe"
Name: "{group}\sayeye suite"; Filename: "{app}\scripts\sayeye.bat"

[Files]
Source: "installer_resources\*"; DestDir: "{app}"; AfterInstall: ConvertConfig('sayeyeController.dll.config'); Flags: ignoreversion recursesubdirs
Source: "temp\*"; DestDir: "{tmp}"; Flags: ignoreversion recursesubdirs

[Run]
Filename: "{tmp}\windowsdesktop-runtime-7.0.9-win-x64.exe"; Description: "install .NET"; StatusMsg:{cm:deps}; Parameters:"/install /quiet /norestart"; Flags: runascurrentuser 64bit skipifdoesntexist waituntilterminated; BeforeInstall: UpdateProgress(0);
Filename: "{tmp}\aspnetcore-runtime-7.0.9-win-x64.exe"; Description: "install AspNET"; StatusMsg:"{cm:deps}"; Parameters:"/install /quiet /norestart"; Flags: runascurrentuser 64bit skipifdoesntexist waituntilterminated; BeforeInstall: UpdateProgress(10);
Filename: "powershell.exe"; Description: "installing wsl"; StatusMsg:"{cm:wsl2}"; Parameters: "-ExecutionPolicy Bypass -File ""{app}\installWsl.ps1"""; WorkingDir: {app}; Flags: runhidden runasoriginaluser 64bit waituntilterminated; BeforeInstall: UpdateProgress(30);
Filename: "schtasks"; Description: "Autostart Sayeye Controller"; Parameters: "/Create /F /RL highest /SC onlogon /TR """"{app}\sayeyeController.exe\"""" /TN ""sayeyeController"""; Flags: runhidden nowait postinstall skipifsilent runascurrentuser; BeforeInstall: UpdateProgress(90);

[UninstallRun]
Filename: "schtasks"; Parameters: "/Delete /TN ""sayeyeController"" /F"; Flags: runhidden runascurrentuser
Filename: "wsl"; Parameters: "--unregister sayeye"; Flags: runhidden runascurrentuser

[Code]

const
  SMTO_ABORTIFHUNG = 2;
  WM_WININICHANGE = $001A;
  WM_SETTINGCHANGE = WM_WININICHANGE;

type
  WPARAM = UINT_PTR;
  LPARAM = INT_PTR;
  LRESULT = INT_PTR;

function SendTextMessageTimeout(hWnd: HWND; Msg: UINT;
  wParam: WPARAM; lParam: PAnsiChar; fuFlags: UINT;
  uTimeout: UINT; out lpdwResult: DWORD): LRESULT;
  external 'SendMessageTimeoutA@user32.dll stdcall';  

procedure InitializeWizard();
begin

if ((NOT RegKeyExists(HKLM64,'SOFTWARE\WOW6432Node\Tobii\Bundle')) AND (NOT RegKeyExists(HKLM64,'SOFTWARE\Tobii\EyeX'))) then 
  begin
  MsgBox('Please install Tobii software first', mbCriticalError, MB_OK);
  Abort;
end;
end;

procedure ConvertConfig(xmlFileName: String);
var
  xmlFile: String;
  xmlInhalt: TArrayOfString;
  strName: String;
  strTest: String;
  tmpConfigFile: String;
  k: Integer;
begin
  xmlFile := ExpandConstant('{app}') + '\' + xmlFileName;
  tmpConfigFile:= ExpandConstant('{app}') + '\config.tmp';
  //strName :=  {app};

  if (FileExists(xmlFile)) then begin
    // Load the file to a String array
    LoadStringsFromFile(xmlFile, xmlInhalt);

    for k:=0 to GetArrayLength(xmlInhalt)-1 do begin
      strTest := xmlInhalt[k];
      if (Pos('key="programFolder"', strTest) <> 0 ) then  begin
        strTest := '  <add key="programFolder" value="' + ExpandConstant('{app}') + '"/> ';
      end;
      SaveStringToFile(tmpConfigFile, strTest + #13#10,  True);
    end;

    DeleteFile(xmlFile); //delete the old exe.config
    RenameFile(tmpConfigFile,xmlFile);
  end;
end;

procedure RefreshEnvironment;
var
  S: AnsiString;
  MsgResult: DWORD;
begin
  S := 'Environment';
  SendTextMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0,
    PAnsiChar(S), SMTO_ABORTIFHUNG, 5000, MsgResult);
end;

//procedure CurStepChanged(CurStep: TSetupStep);
//begin
//    if CurStep = ssPostInstall then 
//    begin
        // Copy downloaded files to application directory
//        FileCopy(ExpandConstant('{tmp}\test1.zip'), ExpandConstant('{app}\test1.zip'), false);
//        FileCopy(ExpandConstant('{tmp}\test2.zip'), ExpandConstant('{app}\test2.zip'), false);
//        FileCopy(ExpandConstant('{tmp}\test3.zip'), ExpandConstant('{app}\test3.zip'), false);
//    end;
//end;

// procedure CurPageChanged(CurPageID: Integer);
// begin
//     if CurPageID = wpSelectTasks then
//     begin
//         TasksListClickCheck(WizardForm.TasksList);
//     end;
// end;
procedure UpdateProgress(Position: Integer);
begin
  WizardForm.ProgressGauge.Position := Position * WizardForm.ProgressGauge.Max div 100;
end;



/////////////////////////////////////////////////////////////////////
function GetUninstallString(): String;
var
  sUnInstPath: String;
  sUnInstallString: String;
begin
  sUnInstPath := ExpandConstant('Software\Microsoft\Windows\CurrentVersion\Uninstall\Sayeye Suite_is1');
  sUnInstallString := '';
  if not RegQueryStringValue(HKLM, sUnInstPath, 'UninstallString', sUnInstallString) then
    RegQueryStringValue(HKCU, sUnInstPath, 'UninstallString', sUnInstallString);
    Log(sUnInstallString)
  Result := sUnInstallString;
end;


/////////////////////////////////////////////////////////////////////
function IsUpgrade(): Boolean;
begin
  Result := (GetUninstallString() <> '');
end;


/////////////////////////////////////////////////////////////////////
function UnInstallOldVersion(): Integer;
var
  sUnInstallString: String;
  stopFile: String;
  iResultCode: Integer;
  sResultCode: Integer;
begin
// Return Values:
// 1 - uninstall string is empty
// 2 - error executing the UnInstallString
// 3 - successfully executed the UnInstallString

  // default return value
  Result := 0;

  // get the uninstall string of the old app
  sUnInstallString := GetUninstallString();
  if sUnInstallString <> '' then begin
    sUnInstallString := RemoveQuotes(sUnInstallString);
    stopFile := ExpandConstant('{app}') + '\sayeye-rails-server\stop_staging_local.bat';
    ExecAsOriginalUser(stopFile,'','',SW_HIDE, ewWaitUntilTerminated, sResultCode);
    if Exec(sUnInstallString, '/SILENT /NORESTART /SUPPRESSMSGBOXES','', SW_HIDE, ewWaitUntilTerminated, iResultCode) then
      Result := 3
    else
      Result := 2;
  end else
    Result := 1;
end;

/////////////////////////////////////////////////////////////////////
procedure CurStepChanged(CurStep: TSetupStep);
begin
  if (CurStep=ssInstall) then
  begin
    if (IsUpgrade()) then
    begin
      Log('uninstall old version');
      UnInstallOldVersion();
    end;
  end;
end;
