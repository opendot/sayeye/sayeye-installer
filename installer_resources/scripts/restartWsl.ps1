$distro = "sayeye"

wsl -t $distro
wsl -d $distro -u root -- /home/sayeye/sayeye-rails-server/run.sh

$wslAddress = wsl.exe -d $distro bash -c "ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'"

netsh interface portproxy reset ipv4
netsh advfirewall firewall delete rule name="WSL Prt4Wrd"

netsh interface portproxy delete v4tov4 listenport=3001 listenaddress=0.0.0.0
netsh interface portproxy add v4tov4 listenport=3001 listenaddress=0.0.0.0 connectaddress=$wslAddress

netsh advfirewall firewall add rule name= "WSL Prt4Wrd" dir=in action=allow protocol=TCP localport=3001
netsh advfirewall firewall add rule name= "WSL Prt4Wrd" dir=out action=allow protocol=TCP localport=3001
