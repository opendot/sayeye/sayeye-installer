wsl --update
wsl --import sayeye $env:USERPROFILE\AppData\Roaming\SayeyeSuite\img sayeye-0.8.2.tar
Remove-Item .\sayeye-0.8.2.tar

netsh advfirewall firewall delete rule name="sayeye_driver.exe"
netsh advfirewall firewall delete rule program="$pwd\sayEye-driver\sayEye_driver.exe"
netsh advfirewall firewall add rule program="$pwd\sayEye-driver\sayEye_driver.exe" name="SayeyeDriver" enable="YES" dir=in action=allow
netsh advfirewall firewall add rule program="$pwd\sayEye-driver\sayEye_driver.exe" name="SayeyeDriver" enable="YES" dir=out action=allow
